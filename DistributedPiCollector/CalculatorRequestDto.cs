using System.Text.Json.Serialization;

namespace DistributedPiCollector;

public class CalculatorRequestDto
{
    [JsonPropertyName("id"), JsonInclude]
    public int Id;

    [JsonPropertyName("precision"), JsonInclude]
    public int Precision;
}