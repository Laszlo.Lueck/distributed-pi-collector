using System.Text.Json.Serialization;

namespace DistributedPiCollector;

public class CalculatorResponseDto
{
    [JsonPropertyName("id"), JsonInclude]
    public int Id;

    [JsonPropertyName("result"), JsonInclude]
    public string? Result;
}