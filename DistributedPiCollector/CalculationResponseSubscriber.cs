using Microsoft.Extensions.Logging;

namespace DistributedPiCollector;

public class CalculationResponseSubscriber(ILogger<CalculationResponseSubscriber> logger)
{
    public async Task OnMessageReceived(CalculatorResponseDto message)
    {
        await Task.Run(() =>
        {
            logger.LogInformation("received {Id} with {Result}", message.Id, message.Result);
        });
    }
}