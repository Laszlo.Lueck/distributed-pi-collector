using Microsoft.Extensions.Logging;
using Silverback.Messaging.Publishing;

namespace DistributedPiCollector;

public class CalculationRequestProducer(ILogger<CalculationRequestProducer> logger, IPublisher publisher) : ICalculationRequestProducer
{
    public async Task PublishAsync(CalculatorRequestDto requestDto)
    {
        try
        {
            logger.LogInformation("send request to calculator-slaves with id {Id}", requestDto.Id);
            await publisher.PublishAsync(requestDto);
        }
        catch (Exception e)
        {
            logger.LogError(e, "an error occured");
        }
    }
}

/*
        try
   {
       logger.LogInformation("send response to publish for id {Id}", responseDto.Id);
       await publisher.PublishAsync<CalculatorResponseDto>(responseDto);
   }
   catch (Exception e)
   {
       logger.LogError(e, "an error occured");
   }
*/