namespace DistributedPiCollector;

public interface ICalculationRequestProducer
{
    Task PublishAsync(CalculatorRequestDto requestDto);
}