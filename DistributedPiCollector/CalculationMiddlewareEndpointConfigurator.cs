using Confluent.Kafka;
using Silverback.Messaging.Configuration;

namespace DistributedPiCollector;

public class CalculationMiddlewareEndpointConfigurator : IEndpointsConfigurator
{
    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints =>
                endpoints.Configure(config => config.BootstrapServers = "PLAINTEXT://kafka.gretzki.ddns.net:9092")
                    .AddInbound(endpoint =>
                        endpoint.ConsumeFrom("calculationresponses").Configure(
                            config =>
                            {
                                config.GroupId = "CalculationResponseGroup";
                                config.AutoOffsetReset = AutoOffsetReset.Earliest;
                            }).DeserializeJson(ser => ser.UseFixedType<CalculatorResponseDto>()))
                    .AddOutbound<CalculatorRequestDto>(endpoint =>
                        endpoint.ProduceTo("calculationrequests")
                            .SerializeAsJson(ser => ser.UseFixedType<CalculatorRequestDto>()))
            );
    }
}