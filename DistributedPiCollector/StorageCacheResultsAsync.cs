using Microsoft.Extensions.Logging;
using Optional;
using StackExchange.Redis;

namespace DistributedPiCollector;

public class StorageCacheResultsAsync(IConnectionMultiplexer connectionMultiplexer) : IStorageCacheAsync<string>
{
    private readonly IDatabase _database = connectionMultiplexer.GetDatabase();

    public async Task<Option<string>> GetItemFromCacheAsync(string id)
    {
        var result = await _database.StringGetAsync(id);
        return result.HasValue ? result.ToString().Some() : Option.None<string>();
    }

    public async Task SetItemToCacheAsync(string id, string value)
    {
        await _database.StringSetAsync(id, value, null, When.NotExists, CommandFlags.None);
    }

    public async Task RemoveItemFromCacheAsync(string id)
    {
        await _database.KeyDeleteAsync(id);
    }
}