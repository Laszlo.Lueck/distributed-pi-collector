﻿using DistributedPiCollector;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using StackExchange.Redis;

var builder = Host.CreateApplicationBuilder();
builder.Logging.AddSimpleConsole(options =>
{
    options.IncludeScopes = false;
    options.SingleLine = true;
    options.TimestampFormat = "[yyy-MM-dd HH:mm:ss.ffff]";
    options.ColorBehavior = LoggerColorBehavior.Enabled;
});
builder.Logging.AddFilter("*", LogLevel.Information);
var options = new ConfigurationOptions()
{
    EndPoints = {
        {
            "192.168.0.35",
            6379
        }
    }
};
var redisConnector = await ConnectionMultiplexer.ConnectAsync(options);
builder.Services.AddSingleton<IConnectionMultiplexer>(redisConnector);
builder.Services.AddSingleton<IStorageCacheAsync<string>, StorageCacheResultsAsync>();
builder.Services.AddScoped<ICalculationRequestProducer, CalculationRequestProducer>();

builder
    .Services
    .AddSilverback()
    .UseModel()
    .WithConnectionToMessageBroker(o => o.AddKafka())
    .AddEndpointsConfigurator<CalculationMiddlewareEndpointConfigurator>()
    .AddScopedSubscriber<CalculationResponseSubscriber>();



var app = builder.Build();

app.Services.GetService<IHostApplicationLifetime>()
    ?.ApplicationStarted.Register( () =>
{
    var foo = new CalculatorStarter(app.Services.GetRequiredService<ICalculationRequestProducer>());
    foo.DoSomeStuffAsync(100, 100).Wait();
    
});


await app.RunAsync();

