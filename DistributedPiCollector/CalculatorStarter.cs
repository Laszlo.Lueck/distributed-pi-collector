using Microsoft.Extensions.Logging;

namespace DistributedPiCollector;

public class CalculatorStarter(ICalculationRequestProducer producer)
{

    public async Task DoSomeStuffAsync(int count, int precision)
    {
        var resultsTask = Enumerable
            .Range(1, 100)
            .Select(async d =>
            {
                var req = new CalculatorRequestDto
                {
                    Id = d,
                    Precision = precision
                };
                await producer.PublishAsync(req);
            });

        await Task.WhenAll(resultsTask);
    }
    
}