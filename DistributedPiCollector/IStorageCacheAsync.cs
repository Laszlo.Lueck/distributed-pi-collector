using Optional;

namespace DistributedPiCollector;

public interface IStorageCacheAsync<T>
{

    Task<Option<T>> GetItemFromCacheAsync(string id);

    Task SetItemToCacheAsync(string id, T value);

    Task RemoveItemFromCacheAsync(string id);

}